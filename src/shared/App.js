import React, { Component } from 'react';
import { Switch, Route, Redirect, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import Navbar from '../client/components/Navbar';
import Home from '../client/containers/CategoryPage';
import Product from '../client/containers/Product';
import Cart from '../client/containers/Cart';
import DropDownMenu from "../client/components/DropDownMenu";

class App extends Component {
    render() {
        return (
            <div>
                <Navbar
                    badgeCount={this.props.badgeCount}
                    categories={this.props.categories}
                    selectedCategory={this.props.selectedCategory}
                />
                <DropDownMenu
                    badgeCount={this.props.badgeCount}
                    categories={this.props.categories}
                    selectedCategory={this.props.selectedCategory}
                />
                <div className="content">
                    <Switch>
                        <Route exact path="/category/:product?" name="Home" component={Home} />
                        <Route path="/product/:name?" name="Product" component={Product} />
                        <Route path="/cart" name="Cart" component={Cart} />
                        <Redirect from="/" to="/category/:product?"/>
                    </Switch>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        categories: state.products.categories,
        selectedCategory: state.products.selectedCategory,
        badgeCount: state.products.badgeCount
    }
};

export default withRouter(connect(mapStateToProps, {})(App));