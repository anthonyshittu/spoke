import axios from 'axios/index';
import {PRODUCTS_TYPES} from './types'

const baseUri = 'http://localhost:3002';

export default {
    fetchAllProducts: function({param = ""}) {
        return dispatch => {
            axios.get(`https://my-json-server.typicode.com/spokeldn/react-test/db`)
                .then(({data}) => {
                        let filteredByCategory = [];
                        data.products.forEach(product => {
                            if (filteredByCategory.indexOf(product.category) === -1) {
                                filteredByCategory.push(product.category);
                            }
                        });
                        dispatch({
                            type: PRODUCTS_TYPES.CATEGORIES_RECEIVED,
                            payload: filteredByCategory,
                            param: param || filteredByCategory[0]});
                        dispatch({type: PRODUCTS_TYPES.PRODUCTS_RECEIVED, payload: data.products});
                        dispatch({type: PRODUCTS_TYPES.CATEGORY_SELECTED, param});
                        dispatch({type: PRODUCTS_TYPES.PRODUCTS_FINISHED, payload: true});
                    }
                )
                .catch(e => {
                    console.warn(e);

                })
        }
    },
    fetchProductsByCategory: function(category = "") {
        return dispatch => {
            dispatch({type: PRODUCTS_TYPES.CATEGORY_SELECTED, param: category});
        }
    },
    filterCategoryProducts: function(category, text) {
        return dispatch => {
            dispatch({type: PRODUCTS_TYPES.FILTER_PRODUCTS, category, text});
        }
    },
    fetchProductDetails: function(category, product) {
        return dispatch => {
            dispatch({type: PRODUCTS_TYPES.FETCH_PRODUCT, category, product});
        }
    },
    addProductToCart: function(category, product) {
        return dispatch => {
            dispatch({type: PRODUCTS_TYPES.ADD_PRODUCT_CART, category, product})
        }
    },
    fetchCart: function(category, product) {
        return dispatch => {
            dispatch({type: PRODUCTS_TYPES.FETCH_CART, category, product});
        }
    },
    deleteProductFromCart: function(category, product) {
        return dispatch => {
            dispatch({type: PRODUCTS_TYPES.DELETE_PRODUCT_CART, category, product})
        }
    },
    addDiscount: function(code) {
        return dispatch => {
            dispatch({type: PRODUCTS_TYPES.ADD_DISCOUNT, code})
        }
    }
}