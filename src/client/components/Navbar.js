import React from 'react';
import { Link } from 'react-router-dom';

const Navbar = (props) => (
    <div className="header">
        <div className="nav">
            <ul>
                {
                    props.categories.map((item, i) => {
                        const url = `/category/${item}`;
                        return <li key={i}>
                                    <Link to={url} className={props.selectedCategory === item ? 'active' : ''}>
                                        {item}
                                    </Link>
                                </li>
                    })
                }
            </ul>
        </div>
        <div className="cart">
            <Link to={`/cart`}>CART<span className="badge">{props.badgeCount > 0 ? props.badgeCount : ''}</span></Link>
        </div>
    </div>
);

export default Navbar;