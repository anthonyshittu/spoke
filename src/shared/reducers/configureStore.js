import {applyMiddleware, createStore} from "redux";
import reduxThunk from "redux-thunk";
import reducers from "./index";

export const configureStore = (initialState = {}) => {
    const createStoreWithMiddleware = applyMiddleware(reduxThunk)(createStore);
    return createStoreWithMiddleware(reducers, initialState);
};