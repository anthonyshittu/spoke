import React, { Component } from 'react';
import { connect } from 'react-redux';
import api_action from '../../shared/actions';
import Categories from '../components/Categories'

class CategoryPage extends Component {

    static fetching ({ dispatch, param }) {
        return [dispatch(api_action.fetchAllProducts({dispatch, param}))];
    }

    componentDidMount() {
        this.props.fetchProductsByCategory(this.props.selectedCategory)
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if ((this.props.match.params.product !== undefined) && prevProps.selectedCategory !== this.props.match.params.product) {
            this.props.fetchProductsByCategory(this.props.match.params.product)
        }
    }

    filterProducts = (event) => {
        this.props.filterCategoryProducts(this.props.selectedCategory, event.target.value)
    };

    render() {
        return (
            <React.Fragment>
                <input
                    type="text"
                    name="productFilter"
                    onChange={this.filterProducts}
                />
                <Categories
                    products={this.props.selectedCategoryData}
                    category={this.props.selectedCategory}
                />
            </React.Fragment>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        products: state.products,
        categories: state.products.categories,
        selectedCategory: state.products.selectedCategory,
        selectedCategoryData: state.products.selectedCategoryData
    }
};

const mapDispatchToProps = (dispatch) => ({
    fetchProductsByCategory: (category = "") => dispatch(api_action.fetchProductsByCategory(category)),
    fetchAllProducts: (param = "") => dispatch(api_action.fetchAllProducts(param)),
    filterCategoryProducts: (category, text) => dispatch(api_action.filterCategoryProducts(category, text))
});

export default connect(mapStateToProps, mapDispatchToProps)(CategoryPage);
