import CategoryPage from '../../client/containers/CategoryPage';

export default [
    {
        path: '*',
        component: CategoryPage,
        exact: true,
        isStatic: true
    },
    {
        path: '/category/:product?',
        component: CategoryPage,
        exact: true,
        isStatic: true
    },
];
