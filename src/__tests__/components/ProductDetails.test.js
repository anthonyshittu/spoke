import React from 'react';
import { shallow, mount } from 'enzyme';
import toJson from 'enzyme-to-json';
import { BrowserRouter as Router } from 'react-router-dom';
import ProductDetails from '../../client/components/ProductDetails';

let products = {
        "title": "Sporty",
        "category": "Polos",
        "description": "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.",
        "amount": 1,
        "price": 61.49,
        "image": "https://dummyimage.com/300x200/9e3d9e/363ead",
        "new": true
    };
const category = 'Trousers';

const addProductCart = jest.fn();
describe('<ProductDetails />', () => {
    test('test product details component with amount', () => {
        const component = mount(
            <Router>
                <ProductDetails
                    {...products}
                    addProductToCart={addProductCart}
                />
            </Router>
        );
        expect(component.find('input').length).toEqual(1)
    });
    test('test product details component with no amount left', () => {
        products = {
            "title": "Sporty",
            "category": "Polos",
            "description": "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.",
            "amount": 0,
            "price": 61.49,
            "image": "https://dummyimage.com/300x200/9e3d9e/363ead",
            "new": true
        };
        const component = mount(
            <Router>
                <ProductDetails
                    {...products}
                    addProductToCart={addProductCart}
                />
            </Router>
        );
        expect(component.find('input').length).toEqual(0)
    });
});