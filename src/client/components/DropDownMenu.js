import React from 'react';
import { Link } from 'react-router-dom';

const DropDownMenu = (props) => (
    <div className="dropdown">
        <button className="dropbtn">MENU</button>
        <div className="dropdown-content">
            {
                props.categories.map((item, i) => {
                    const url = `/category/${item}`;
                    return <Link to={url} key={i} className={props.selectedCategory === item ? 'active' : ''}>
                            {item}
                        </Link>
                })
            }
            <Link to={`/cart`}>CART <span className="badge">{props.badgeCount > 0 ? props.badgeCount : ''}</span></Link>
        </div>
    </div>
);

export default DropDownMenu;