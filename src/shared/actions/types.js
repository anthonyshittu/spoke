export const PRODUCTS_TYPES = {
    PRODUCTS_REQUESTED: 'products_requested',
    PRODUCTS_RECEIVED: 'products_received',
    PRODUCTS_FINISHED: 'products_finished',
    CATEGORIES_RECEIVED: 'categories_received',
    CATEGORY_SELECTED: 'category_selected',
    UPDATE_DATA: 'update_data',
    FILTER_PRODUCTS: 'filter_products',
    FETCH_PRODUCT: 'fetch_product',
    ADD_PRODUCT_CART: 'add_product_cart',
    FETCH_CART: 'fetch_cart',
    DELETE_PRODUCT_CART: 'delete_product_cart',
    ADD_DISCOUNT: 'add_product'
};