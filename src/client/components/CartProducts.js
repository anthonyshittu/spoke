import React, { useState } from "react";
import Image from './Image';

const CartProducts = ({ cart, cartTotal, discountCartTotal, deleteProductFromCart, discountCode }) => (
    <div className="container">
        <CartList
            cart={cart}
            cartTotal={cartTotal}
            discountCartTotal={discountCartTotal}
            deleteProductFromCart={deleteProductFromCart}
            discountCode={discountCode} />
    </div>
);

const CartList = ({ cart, cartTotal, discountCode, deleteProductFromCart }) => {
        const [total, setValue] = useState(cartTotal);
        const calculateDiscount = (event) => {
            if (event.target.value === discountCode) {
                const discountTotal = cartTotal -(cartTotal * .20).toFixed(2)
                setValue(discountTotal.toFixed(2));
            } else {
                setValue(cartTotal);
            }
        };
        return cart.length === 0 ?
            <div>Your basket is empty</div>
            :
            <div className="table">
                {
                    cart.map(obj => <CartRow key={obj.title} productDetails={obj} deleteProductFromCart={deleteProductFromCart}/>)
                }
                <div className="row">
                    <div className="cell last-cell">

                    </div>
                    <div className="cell last-cell">
                    </div>
                    <div className="cell last-cell">
                        <input type="text"  placeholder="Discount" onChange={calculateDiscount} />
                    </div>
                    <div className="cell last-cell">
                        £{total}
                    </div>
                    <div className="cell last-cell">
                        <input  type="button" value="Pay"  className="btn default"/>
                    </div>
                </div>
            </div>
};

const CartRow = ({ productDetails, deleteProductFromCart }) => {
    return <div className="row">
        <div className="cell">
            <Image url={productDetails.image} alt={productDetails.title} width={100} height={100}/>
        </div>
        <div className="cell">
            {productDetails.title}
        </div>
        <div className="cell">
            {productDetails.quatity} Pieces
        </div>
        <div className="cell">
            £{productDetails.quatity_total}
        </div>
        <div className="cell">
            <input type="button" value="DELETE" className="btn default" onClick={() => {
                deleteProductFromCart(productDetails.category, productDetails.title, productDetails.quatity)
            }}/>
        </div>
    </div>
};

export default CartProducts;