import React, { Component } from 'react';
import { connect } from 'react-redux';
import ProductDetails from '../components/ProductDetails';
import api_action from '../../shared/actions';

class Product extends Component {

    componentDidMount() {
        this.props.fetchProductDetails(this.props.selectedCategory, this.props.match.params.name);
    }

    addProductCart = () => {
        this.props.addProductToCart(this.props.selectedCategory, this.props.match.params.name);
    };

    render() {
        return (
            <ProductDetails {...this.props.productDetails} addProductToCart={this.addProductCart} />
        );
    }
}

const mapStateToProps = (state) => {
    return {
        selectedCategory: state.products.selectedCategory,
        productDetails: state.products.productDetails,
    }
};

const mapDispatchToProps = (dispatch) => ({
    fetchProductDetails: (category, product) => dispatch(api_action.fetchProductDetails(category, product)),
    addProductToCart: (category, product) => dispatch(api_action.addProductToCart(category, product)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Product);
