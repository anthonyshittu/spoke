import React from "react";
import {Link} from "react-router-dom";

const CategoryProducts = ({ products, category }) => (
    <div className="container">
        <ProductList products={products} category={category} />
    </div>
);

const ProductList = ({ products, category }) => (

        <div className="table">
            {
                products.filter(product => product.category === category)
                    .map(obj => <ProductRow key={obj.title} productDetails={obj} />)
            }
        </div>
);

const ProductRow = ({ productDetails }) => (
    <div className="row">
        <div className="cell">
            {
                (productDetails.new === true) ?
                    <Link to={`/product/${productDetails.title}`}>{productDetails.title} - NEW</Link>
                    :
                     <Link to={`/product/${productDetails.title}`}>{productDetails.title}</Link>
            }
        </div>
    </div>
);

export default CategoryProducts;