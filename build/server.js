/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/server/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/client/components/CartProducts.js":
/*!***********************************************!*\
  !*** ./src/client/components/CartProducts.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/slicedToArray */ \"@babel/runtime/helpers/slicedToArray\");\n/* harmony import */ var _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ \"react\");\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var _Image__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Image */ \"./src/client/components/Image.js\");\n\n\n\n\nvar CartProducts = function CartProducts(_ref) {\n  var cart = _ref.cart,\n      cartTotal = _ref.cartTotal,\n      discountCartTotal = _ref.discountCartTotal,\n      deleteProductFromCart = _ref.deleteProductFromCart,\n      discountCode = _ref.discountCode;\n  return react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(\"div\", {\n    className: \"container\"\n  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(CartList, {\n    cart: cart,\n    cartTotal: cartTotal,\n    discountCartTotal: discountCartTotal,\n    deleteProductFromCart: deleteProductFromCart,\n    discountCode: discountCode\n  }));\n};\n\nvar CartList = function CartList(_ref2) {\n  var cart = _ref2.cart,\n      cartTotal = _ref2.cartTotal,\n      discountCode = _ref2.discountCode,\n      deleteProductFromCart = _ref2.deleteProductFromCart;\n\n  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_1__[\"useState\"])(cartTotal),\n      _useState2 = _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_0___default()(_useState, 2),\n      total = _useState2[0],\n      setValue = _useState2[1];\n\n  var calculateDiscount = function calculateDiscount(event) {\n    if (event.target.value === discountCode) {\n      var discountTotal = cartTotal - (cartTotal * .20).toFixed(2);\n      setValue(discountTotal.toFixed(2));\n    } else {\n      setValue(cartTotal);\n    }\n  };\n\n  return cart.length === 0 ? react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(\"div\", null, \"Your basket is empty\") : react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(\"div\", {\n    className: \"table\"\n  }, cart.map(function (obj) {\n    return react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(CartRow, {\n      key: obj.title,\n      productDetails: obj,\n      deleteProductFromCart: deleteProductFromCart\n    });\n  }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(\"div\", {\n    className: \"row\"\n  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(\"div\", {\n    className: \"cell last-cell\"\n  }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(\"div\", {\n    className: \"cell last-cell\"\n  }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(\"div\", {\n    className: \"cell last-cell\"\n  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(\"input\", {\n    type: \"text\",\n    placeholder: \"Discount\",\n    onChange: calculateDiscount\n  })), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(\"div\", {\n    className: \"cell last-cell\"\n  }, \"\\xA3\", total), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(\"div\", {\n    className: \"cell last-cell\"\n  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(\"input\", {\n    type: \"button\",\n    value: \"Pay\",\n    className: \"btn default\"\n  }))));\n};\n\nvar CartRow = function CartRow(_ref3) {\n  var productDetails = _ref3.productDetails,\n      deleteProductFromCart = _ref3.deleteProductFromCart;\n  return react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(\"div\", {\n    className: \"row\"\n  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(\"div\", {\n    className: \"cell\"\n  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_Image__WEBPACK_IMPORTED_MODULE_2__[\"default\"], {\n    url: productDetails.image,\n    alt: productDetails.title,\n    width: 100,\n    height: 100\n  })), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(\"div\", {\n    className: \"cell\"\n  }, productDetails.title), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(\"div\", {\n    className: \"cell\"\n  }, productDetails.quatity, \" Pieces\"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(\"div\", {\n    className: \"cell\"\n  }, \"\\xA3\", productDetails.quatity_total), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(\"div\", {\n    className: \"cell\"\n  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(\"input\", {\n    type: \"button\",\n    value: \"DELETE\",\n    className: \"btn default\",\n    onClick: function onClick() {\n      deleteProductFromCart(productDetails.category, productDetails.title, productDetails.quatity);\n    }\n  })));\n};\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (CartProducts);\n\n//# sourceURL=webpack:///./src/client/components/CartProducts.js?");

/***/ }),

/***/ "./src/client/components/Categories.js":
/*!*********************************************!*\
  !*** ./src/client/components/Categories.js ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ \"react\");\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ \"react-router-dom\");\n/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_router_dom__WEBPACK_IMPORTED_MODULE_1__);\n\n\n\nvar CategoryProducts = function CategoryProducts(_ref) {\n  var products = _ref.products,\n      category = _ref.category;\n  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"div\", {\n    className: \"container\"\n  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(ProductList, {\n    products: products,\n    category: category\n  }));\n};\n\nvar ProductList = function ProductList(_ref2) {\n  var products = _ref2.products,\n      category = _ref2.category;\n  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"div\", {\n    className: \"table\"\n  }, products.filter(function (product) {\n    return product.category === category;\n  }).map(function (obj) {\n    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(ProductRow, {\n      key: obj.title,\n      productDetails: obj\n    });\n  }));\n};\n\nvar ProductRow = function ProductRow(_ref3) {\n  var productDetails = _ref3.productDetails;\n  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"div\", {\n    className: \"row\"\n  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"div\", {\n    className: \"cell\"\n  }, productDetails[\"new\"] === true ? react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__[\"Link\"], {\n    to: \"/product/\".concat(productDetails.title)\n  }, productDetails.title, \" - NEW\") : react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__[\"Link\"], {\n    to: \"/product/\".concat(productDetails.title)\n  }, productDetails.title)));\n};\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (CategoryProducts);\n\n//# sourceURL=webpack:///./src/client/components/Categories.js?");

/***/ }),

/***/ "./src/client/components/DropDownMenu.js":
/*!***********************************************!*\
  !*** ./src/client/components/DropDownMenu.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ \"react\");\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ \"react-router-dom\");\n/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_router_dom__WEBPACK_IMPORTED_MODULE_1__);\n\n\n\nvar DropDownMenu = function DropDownMenu(props) {\n  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"div\", {\n    className: \"dropdown\"\n  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"button\", {\n    className: \"dropbtn\"\n  }, \"MENU\"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"div\", {\n    className: \"dropdown-content\"\n  }, props.categories.map(function (item, i) {\n    var url = \"/category/\".concat(item);\n    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__[\"Link\"], {\n      to: url,\n      key: i,\n      className: props.selectedCategory === item ? 'active' : ''\n    }, item);\n  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__[\"Link\"], {\n    to: \"/cart\"\n  }, \"CART \", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"span\", {\n    className: \"badge\"\n  }, props.badgeCount > 0 ? props.badgeCount : ''))));\n};\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (DropDownMenu);\n\n//# sourceURL=webpack:///./src/client/components/DropDownMenu.js?");

/***/ }),

/***/ "./src/client/components/Image.js":
/*!****************************************!*\
  !*** ./src/client/components/Image.js ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ \"react\");\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);\n\n\nvar Image = function Image(_ref) {\n  var alt = _ref.alt,\n      url = _ref.url,\n      width = _ref.width,\n      height = _ref.height;\n  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"img\", {\n    src: url,\n    width: width,\n    height: height,\n    alt: alt\n  });\n};\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (Image);\n\n//# sourceURL=webpack:///./src/client/components/Image.js?");

/***/ }),

/***/ "./src/client/components/Navbar.js":
/*!*****************************************!*\
  !*** ./src/client/components/Navbar.js ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ \"react\");\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ \"react-router-dom\");\n/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_router_dom__WEBPACK_IMPORTED_MODULE_1__);\n\n\n\nvar Navbar = function Navbar(props) {\n  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"div\", {\n    className: \"header\"\n  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"div\", {\n    className: \"nav\"\n  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"ul\", null, props.categories.map(function (item, i) {\n    var url = \"/category/\".concat(item);\n    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"li\", {\n      key: i\n    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__[\"Link\"], {\n      to: url,\n      className: props.selectedCategory === item ? 'active' : ''\n    }, item));\n  }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"div\", {\n    className: \"cart\"\n  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__[\"Link\"], {\n    to: \"/cart\"\n  }, \"CART\", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"span\", {\n    className: \"badge\"\n  }, props.badgeCount > 0 ? props.badgeCount : ''))));\n};\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (Navbar);\n\n//# sourceURL=webpack:///./src/client/components/Navbar.js?");

/***/ }),

/***/ "./src/client/components/ProductDetails.js":
/*!*************************************************!*\
  !*** ./src/client/components/ProductDetails.js ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/slicedToArray */ \"@babel/runtime/helpers/slicedToArray\");\n/* harmony import */ var _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ \"react\");\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var _Image__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Image */ \"./src/client/components/Image.js\");\n\n\n\n\nfunction ProductDetails(_ref) {\n  var title = _ref.title,\n      price = _ref.price,\n      description = _ref.description,\n      image = _ref.image,\n      amount = _ref.amount,\n      addProductToCart = _ref.addProductToCart;\n\n  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_1__[\"useState\"])(false),\n      _useState2 = _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_0___default()(_useState, 2),\n      ellipsis = _useState2[0],\n      setValue = _useState2[1];\n\n  return title ? react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(\"div\", {\n    className: \"product-container\"\n  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(\"div\", {\n    className: \"product-thumb\"\n  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_Image__WEBPACK_IMPORTED_MODULE_2__[\"default\"], {\n    url: image,\n    alt: title,\n    width: 200,\n    height: 200\n  })), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(\"div\", {\n    className: \"product-content\"\n  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(\"h3\", {\n    className: \"product-title\"\n  }, \"\\xA3\", price), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(\"div\", {\n    className: ellipsis ? \"product-desc product-desc-no-ellipsis\" : \"product-desc product-desc-ellipsis\",\n    onClick: function onClick() {\n      return setValue(!ellipsis);\n    }\n  }, description), amount === 0 ? react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(\"p\", null, \"Stock not available\") : react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(\"input\", {\n    type: \"button\",\n    value: \"Add to Cart\",\n    className: \"btn default\",\n    onClick: function onClick() {\n      addProductToCart();\n    }\n  }))) : react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(\"p\", null, \"An error as occurred\");\n}\n\n;\n/* harmony default export */ __webpack_exports__[\"default\"] = (ProductDetails);\n\n//# sourceURL=webpack:///./src/client/components/ProductDetails.js?");

/***/ }),

/***/ "./src/client/containers/Cart.js":
/*!***************************************!*\
  !*** ./src/client/containers/Cart.js ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/classCallCheck */ \"@babel/runtime/helpers/classCallCheck\");\n/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/helpers/createClass */ \"@babel/runtime/helpers/createClass\");\n/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime/helpers/possibleConstructorReturn */ \"@babel/runtime/helpers/possibleConstructorReturn\");\n/* harmony import */ var _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__);\n/* harmony import */ var _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @babel/runtime/helpers/getPrototypeOf */ \"@babel/runtime/helpers/getPrototypeOf\");\n/* harmony import */ var _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_3__);\n/* harmony import */ var _babel_runtime_helpers_assertThisInitialized__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @babel/runtime/helpers/assertThisInitialized */ \"@babel/runtime/helpers/assertThisInitialized\");\n/* harmony import */ var _babel_runtime_helpers_assertThisInitialized__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_assertThisInitialized__WEBPACK_IMPORTED_MODULE_4__);\n/* harmony import */ var _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @babel/runtime/helpers/inherits */ \"@babel/runtime/helpers/inherits\");\n/* harmony import */ var _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_5__);\n/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @babel/runtime/helpers/defineProperty */ \"@babel/runtime/helpers/defineProperty\");\n/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_6__);\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! react */ \"react\");\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_7__);\n/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! react-redux */ \"react-redux\");\n/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(react_redux__WEBPACK_IMPORTED_MODULE_8__);\n/* harmony import */ var _components_CartProducts__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../components/CartProducts */ \"./src/client/components/CartProducts.js\");\n/* harmony import */ var _shared_actions__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../shared/actions */ \"./src/shared/actions/index.js\");\n\n\n\n\n\n\n\n\n\n\n\n\nvar Cart =\n/*#__PURE__*/\nfunction (_Component) {\n  _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_5___default()(Cart, _Component);\n\n  function Cart() {\n    var _getPrototypeOf2;\n\n    var _this;\n\n    _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default()(this, Cart);\n\n    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {\n      args[_key] = arguments[_key];\n    }\n\n    _this = _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2___default()(this, (_getPrototypeOf2 = _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_3___default()(Cart)).call.apply(_getPrototypeOf2, [this].concat(args)));\n\n    _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_6___default()(_babel_runtime_helpers_assertThisInitialized__WEBPACK_IMPORTED_MODULE_4___default()(_this), \"discountPurchase\", function (event) {\n      _this.props.addDiscount(event.target.value);\n    });\n\n    _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_6___default()(_babel_runtime_helpers_assertThisInitialized__WEBPACK_IMPORTED_MODULE_4___default()(_this), \"removeProduct\", function (catgegory, product, quatity) {\n      _this.props.deleteProductFromCart(catgegory, product, quatity);\n    });\n\n    return _this;\n  }\n\n  _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1___default()(Cart, [{\n    key: \"render\",\n    value: function render() {\n      return react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(_components_CartProducts__WEBPACK_IMPORTED_MODULE_9__[\"default\"], {\n        cart: this.props.cart,\n        cartTotal: this.props.cartTotal,\n        discountCartTotal: this.props.discountCartTotal,\n        discountCode: this.props.discountCode,\n        deleteProductFromCart: this.removeProduct,\n        addDiscount: this.discountPurchase\n      });\n    }\n  }]);\n\n  return Cart;\n}(react__WEBPACK_IMPORTED_MODULE_7__[\"Component\"]);\n\nvar mapStateToProps = function mapStateToProps(state) {\n  return {\n    cart: state.products.cart,\n    cartTotal: state.products.cartTotal,\n    discountCartTotal: state.products.discountCartTotal,\n    discountCode: state.products.discountCode\n  };\n};\n\nvar mapDispatchToProps = function mapDispatchToProps(dispatch) {\n  return {\n    deleteProductFromCart: function deleteProductFromCart(category, product, quatity) {\n      return dispatch(_shared_actions__WEBPACK_IMPORTED_MODULE_10__[\"default\"].deleteProductFromCart(category, product, quatity));\n    },\n    addDiscount: function addDiscount(code) {\n      return dispatch(_shared_actions__WEBPACK_IMPORTED_MODULE_10__[\"default\"].addDiscount(code));\n    }\n  };\n};\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (Object(react_redux__WEBPACK_IMPORTED_MODULE_8__[\"connect\"])(mapStateToProps, mapDispatchToProps)(Cart));\n\n//# sourceURL=webpack:///./src/client/containers/Cart.js?");

/***/ }),

/***/ "./src/client/containers/CategoryPage.js":
/*!***********************************************!*\
  !*** ./src/client/containers/CategoryPage.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/classCallCheck */ \"@babel/runtime/helpers/classCallCheck\");\n/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/helpers/createClass */ \"@babel/runtime/helpers/createClass\");\n/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime/helpers/possibleConstructorReturn */ \"@babel/runtime/helpers/possibleConstructorReturn\");\n/* harmony import */ var _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__);\n/* harmony import */ var _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @babel/runtime/helpers/getPrototypeOf */ \"@babel/runtime/helpers/getPrototypeOf\");\n/* harmony import */ var _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_3__);\n/* harmony import */ var _babel_runtime_helpers_assertThisInitialized__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @babel/runtime/helpers/assertThisInitialized */ \"@babel/runtime/helpers/assertThisInitialized\");\n/* harmony import */ var _babel_runtime_helpers_assertThisInitialized__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_assertThisInitialized__WEBPACK_IMPORTED_MODULE_4__);\n/* harmony import */ var _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @babel/runtime/helpers/inherits */ \"@babel/runtime/helpers/inherits\");\n/* harmony import */ var _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_5__);\n/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @babel/runtime/helpers/defineProperty */ \"@babel/runtime/helpers/defineProperty\");\n/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_6__);\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! react */ \"react\");\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_7__);\n/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! react-redux */ \"react-redux\");\n/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(react_redux__WEBPACK_IMPORTED_MODULE_8__);\n/* harmony import */ var _shared_actions__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../shared/actions */ \"./src/shared/actions/index.js\");\n/* harmony import */ var _components_Categories__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../components/Categories */ \"./src/client/components/Categories.js\");\n\n\n\n\n\n\n\n\n\n\n\n\nvar CategoryPage =\n/*#__PURE__*/\nfunction (_Component) {\n  _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_5___default()(CategoryPage, _Component);\n\n  function CategoryPage() {\n    var _getPrototypeOf2;\n\n    var _this;\n\n    _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default()(this, CategoryPage);\n\n    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {\n      args[_key] = arguments[_key];\n    }\n\n    _this = _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2___default()(this, (_getPrototypeOf2 = _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_3___default()(CategoryPage)).call.apply(_getPrototypeOf2, [this].concat(args)));\n\n    _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_6___default()(_babel_runtime_helpers_assertThisInitialized__WEBPACK_IMPORTED_MODULE_4___default()(_this), \"filterProducts\", function (event) {\n      _this.props.filterCategoryProducts(_this.props.selectedCategory, event.target.value);\n    });\n\n    return _this;\n  }\n\n  _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1___default()(CategoryPage, [{\n    key: \"componentDidMount\",\n    value: function componentDidMount() {\n      this.props.fetchProductsByCategory(this.props.selectedCategory);\n    }\n  }, {\n    key: \"componentDidUpdate\",\n    value: function componentDidUpdate(prevProps, prevState, snapshot) {\n      if (this.props.match.params.product !== undefined && prevProps.selectedCategory !== this.props.match.params.product) {\n        this.props.fetchProductsByCategory(this.props.match.params.product);\n      }\n    }\n  }, {\n    key: \"render\",\n    value: function render() {\n      return react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_7___default.a.Fragment, null, react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(\"input\", {\n        type: \"text\",\n        name: \"productFilter\",\n        onChange: this.filterProducts\n      }), react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(_components_Categories__WEBPACK_IMPORTED_MODULE_10__[\"default\"], {\n        products: this.props.selectedCategoryData,\n        category: this.props.selectedCategory\n      }));\n    }\n  }], [{\n    key: \"fetching\",\n    value: function fetching(_ref) {\n      var dispatch = _ref.dispatch,\n          param = _ref.param;\n      return [dispatch(_shared_actions__WEBPACK_IMPORTED_MODULE_9__[\"default\"].fetchAllProducts({\n        dispatch: dispatch,\n        param: param\n      }))];\n    }\n  }]);\n\n  return CategoryPage;\n}(react__WEBPACK_IMPORTED_MODULE_7__[\"Component\"]);\n\nvar mapStateToProps = function mapStateToProps(state) {\n  return {\n    products: state.products,\n    categories: state.products.categories,\n    selectedCategory: state.products.selectedCategory,\n    selectedCategoryData: state.products.selectedCategoryData\n  };\n};\n\nvar mapDispatchToProps = function mapDispatchToProps(dispatch) {\n  return {\n    fetchProductsByCategory: function fetchProductsByCategory() {\n      var category = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : \"\";\n      return dispatch(_shared_actions__WEBPACK_IMPORTED_MODULE_9__[\"default\"].fetchProductsByCategory(category));\n    },\n    fetchAllProducts: function fetchAllProducts() {\n      var param = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : \"\";\n      return dispatch(_shared_actions__WEBPACK_IMPORTED_MODULE_9__[\"default\"].fetchAllProducts(param));\n    },\n    filterCategoryProducts: function filterCategoryProducts(category, text) {\n      return dispatch(_shared_actions__WEBPACK_IMPORTED_MODULE_9__[\"default\"].filterCategoryProducts(category, text));\n    }\n  };\n};\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (Object(react_redux__WEBPACK_IMPORTED_MODULE_8__[\"connect\"])(mapStateToProps, mapDispatchToProps)(CategoryPage));\n\n//# sourceURL=webpack:///./src/client/containers/CategoryPage.js?");

/***/ }),

/***/ "./src/client/containers/Product.js":
/*!******************************************!*\
  !*** ./src/client/containers/Product.js ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/extends */ \"@babel/runtime/helpers/extends\");\n/* harmony import */ var _babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/helpers/classCallCheck */ \"@babel/runtime/helpers/classCallCheck\");\n/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime/helpers/createClass */ \"@babel/runtime/helpers/createClass\");\n/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_2__);\n/* harmony import */ var _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @babel/runtime/helpers/possibleConstructorReturn */ \"@babel/runtime/helpers/possibleConstructorReturn\");\n/* harmony import */ var _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__);\n/* harmony import */ var _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @babel/runtime/helpers/getPrototypeOf */ \"@babel/runtime/helpers/getPrototypeOf\");\n/* harmony import */ var _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__);\n/* harmony import */ var _babel_runtime_helpers_assertThisInitialized__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @babel/runtime/helpers/assertThisInitialized */ \"@babel/runtime/helpers/assertThisInitialized\");\n/* harmony import */ var _babel_runtime_helpers_assertThisInitialized__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_assertThisInitialized__WEBPACK_IMPORTED_MODULE_5__);\n/* harmony import */ var _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @babel/runtime/helpers/inherits */ \"@babel/runtime/helpers/inherits\");\n/* harmony import */ var _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_6__);\n/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @babel/runtime/helpers/defineProperty */ \"@babel/runtime/helpers/defineProperty\");\n/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_7__);\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! react */ \"react\");\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_8__);\n/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! react-redux */ \"react-redux\");\n/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(react_redux__WEBPACK_IMPORTED_MODULE_9__);\n/* harmony import */ var _components_ProductDetails__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../components/ProductDetails */ \"./src/client/components/ProductDetails.js\");\n/* harmony import */ var _shared_actions__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../shared/actions */ \"./src/shared/actions/index.js\");\n\n\n\n\n\n\n\n\n\n\n\n\n\nvar Product =\n/*#__PURE__*/\nfunction (_Component) {\n  _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_6___default()(Product, _Component);\n\n  function Product() {\n    var _getPrototypeOf2;\n\n    var _this;\n\n    _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1___default()(this, Product);\n\n    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {\n      args[_key] = arguments[_key];\n    }\n\n    _this = _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3___default()(this, (_getPrototypeOf2 = _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4___default()(Product)).call.apply(_getPrototypeOf2, [this].concat(args)));\n\n    _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_7___default()(_babel_runtime_helpers_assertThisInitialized__WEBPACK_IMPORTED_MODULE_5___default()(_this), \"addProductCart\", function () {\n      _this.props.addProductToCart(_this.props.selectedCategory, _this.props.match.params.name);\n    });\n\n    return _this;\n  }\n\n  _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_2___default()(Product, [{\n    key: \"componentDidMount\",\n    value: function componentDidMount() {\n      this.props.fetchProductDetails(this.props.selectedCategory, this.props.match.params.name);\n    }\n  }, {\n    key: \"render\",\n    value: function render() {\n      return react__WEBPACK_IMPORTED_MODULE_8___default.a.createElement(_components_ProductDetails__WEBPACK_IMPORTED_MODULE_10__[\"default\"], _babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default()({}, this.props.productDetails, {\n        addProductToCart: this.addProductCart\n      }));\n    }\n  }]);\n\n  return Product;\n}(react__WEBPACK_IMPORTED_MODULE_8__[\"Component\"]);\n\nvar mapStateToProps = function mapStateToProps(state) {\n  return {\n    selectedCategory: state.products.selectedCategory,\n    productDetails: state.products.productDetails\n  };\n};\n\nvar mapDispatchToProps = function mapDispatchToProps(dispatch) {\n  return {\n    fetchProductDetails: function fetchProductDetails(category, product) {\n      return dispatch(_shared_actions__WEBPACK_IMPORTED_MODULE_11__[\"default\"].fetchProductDetails(category, product));\n    },\n    addProductToCart: function addProductToCart(category, product) {\n      return dispatch(_shared_actions__WEBPACK_IMPORTED_MODULE_11__[\"default\"].addProductToCart(category, product));\n    }\n  };\n};\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (Object(react_redux__WEBPACK_IMPORTED_MODULE_9__[\"connect\"])(mapStateToProps, mapDispatchToProps)(Product));\n\n//# sourceURL=webpack:///./src/client/containers/Product.js?");

/***/ }),

/***/ "./src/server/index.js":
/*!*****************************!*\
  !*** ./src/server/index.js ***!
  \*****************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ \"@babel/runtime/regenerator\");\n/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _babel_runtime_helpers_objectSpread__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/helpers/objectSpread */ \"@babel/runtime/helpers/objectSpread\");\n/* harmony import */ var _babel_runtime_helpers_objectSpread__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_objectSpread__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime/helpers/asyncToGenerator */ \"@babel/runtime/helpers/asyncToGenerator\");\n/* harmony import */ var _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2__);\n/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! express */ \"express\");\n/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(express__WEBPACK_IMPORTED_MODULE_3__);\n/* harmony import */ var cors__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! cors */ \"cors\");\n/* harmony import */ var cors__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(cors__WEBPACK_IMPORTED_MODULE_4__);\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react */ \"react\");\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_5__);\n/* harmony import */ var react_dom_server__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react-dom/server */ \"react-dom/server\");\n/* harmony import */ var react_dom_server__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(react_dom_server__WEBPACK_IMPORTED_MODULE_6__);\n/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! react-router-dom */ \"react-router-dom\");\n/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(react_router_dom__WEBPACK_IMPORTED_MODULE_7__);\n/* harmony import */ var path__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! path */ \"path\");\n/* harmony import */ var path__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(path__WEBPACK_IMPORTED_MODULE_8__);\n/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! react-redux */ \"react-redux\");\n/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(react_redux__WEBPACK_IMPORTED_MODULE_9__);\n/* harmony import */ var _shared_reducers_configureStore__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../shared/reducers/configureStore */ \"./src/shared/reducers/configureStore.js\");\n/* harmony import */ var _shared_App__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../shared/App */ \"./src/shared/App.js\");\n/* harmony import */ var _shared_router_routes__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../shared/router/routes */ \"./src/shared/router/routes.js\");\n/* harmony import */ var _template__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./template */ \"./src/server/template.js\");\n\n\n\n\n\n\n\n\n\n\n\n\n\n\nvar app = express__WEBPACK_IMPORTED_MODULE_3___default()();\napp.use(cors__WEBPACK_IMPORTED_MODULE_4___default()());\napp.use(express__WEBPACK_IMPORTED_MODULE_3___default.a[\"static\"]('public'));\n\nvar delay = function delay(ms) {\n  return new Promise(function (res) {\n    return setTimeout(res, ms);\n  });\n};\n\napp.get('/favicon.ico', function (req, res) {\n  return res.status(204);\n});\napp.get('*',\n/*#__PURE__*/\nfunction () {\n  var _ref = _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2___default()(\n  /*#__PURE__*/\n  _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee(req, res, next) {\n    var store, activeRoute, param, markup;\n    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {\n      while (1) {\n        switch (_context.prev = _context.next) {\n          case 0:\n            store = Object(_shared_reducers_configureStore__WEBPACK_IMPORTED_MODULE_10__[\"configureStore\"])();\n            activeRoute = _shared_router_routes__WEBPACK_IMPORTED_MODULE_12__[\"default\"].find(function (path) {\n              return Object(react_router_dom__WEBPACK_IMPORTED_MODULE_7__[\"matchPath\"])(req.path, path);\n            }) || {};\n\n            if (req.params[0].split(\"/\")[2] !== 'undefined') {\n              param = req.params[0].split(\"/\")[2];\n            }\n\n            if ('isStatic' in activeRoute) {\n              activeRoute.component.fetching(_babel_runtime_helpers_objectSpread__WEBPACK_IMPORTED_MODULE_1___default()({}, store, {\n                param: param\n              }));\n            }\n\n            _context.next = 6;\n            return delay(1000);\n\n          case 6:\n            markup = Object(react_dom_server__WEBPACK_IMPORTED_MODULE_6__[\"renderToString\"])(react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement(react_redux__WEBPACK_IMPORTED_MODULE_9__[\"Provider\"], {\n              store: store\n            }, react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_7__[\"StaticRouter\"], {\n              location: req.url,\n              context: store.getState()\n            }, react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement(_shared_App__WEBPACK_IMPORTED_MODULE_11__[\"default\"], null))));\n            res.send(Object(_template__WEBPACK_IMPORTED_MODULE_13__[\"default\"])(store, markup));\n\n          case 8:\n          case \"end\":\n            return _context.stop();\n        }\n      }\n    }, _callee);\n  }));\n\n  return function (_x, _x2, _x3) {\n    return _ref.apply(this, arguments);\n  };\n}());\nvar PORT = 2040;\napp.listen(PORT, function () {\n  console.log(\"Server is listening on port \".concat(PORT));\n});\n\n//# sourceURL=webpack:///./src/server/index.js?");

/***/ }),

/***/ "./src/server/template.js":
/*!********************************!*\
  !*** ./src/server/template.js ***!
  \********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony default export */ __webpack_exports__[\"default\"] = (function (store, markup) {\n  return \"\\n  <!DOCTYPE html>\\n  <html>\\n    <head>\\n      <script>window.__SERIALIZED_DATA__ = \".concat(JSON.stringify(store.getState()), \"</script>\\n    </head>\\n    <body>\\n      <div id=\\\"app\\\">\").concat(markup, \"</div>\\n      <script src=\\\"/bundle.js\\\" type=\\\"text/javascript\\\"></script>\\n    </body>\\n  </html>\\n\");\n});\n\n//# sourceURL=webpack:///./src/server/template.js?");

/***/ }),

/***/ "./src/shared/App.js":
/*!***************************!*\
  !*** ./src/shared/App.js ***!
  \***************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/classCallCheck */ \"@babel/runtime/helpers/classCallCheck\");\n/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/helpers/createClass */ \"@babel/runtime/helpers/createClass\");\n/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime/helpers/possibleConstructorReturn */ \"@babel/runtime/helpers/possibleConstructorReturn\");\n/* harmony import */ var _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__);\n/* harmony import */ var _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @babel/runtime/helpers/getPrototypeOf */ \"@babel/runtime/helpers/getPrototypeOf\");\n/* harmony import */ var _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_3__);\n/* harmony import */ var _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @babel/runtime/helpers/inherits */ \"@babel/runtime/helpers/inherits\");\n/* harmony import */ var _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_4__);\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react */ \"react\");\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_5__);\n/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react-router-dom */ \"react-router-dom\");\n/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(react_router_dom__WEBPACK_IMPORTED_MODULE_6__);\n/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! react-redux */ \"react-redux\");\n/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(react_redux__WEBPACK_IMPORTED_MODULE_7__);\n/* harmony import */ var _client_components_Navbar__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../client/components/Navbar */ \"./src/client/components/Navbar.js\");\n/* harmony import */ var _client_containers_CategoryPage__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../client/containers/CategoryPage */ \"./src/client/containers/CategoryPage.js\");\n/* harmony import */ var _client_containers_Product__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../client/containers/Product */ \"./src/client/containers/Product.js\");\n/* harmony import */ var _client_containers_Cart__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../client/containers/Cart */ \"./src/client/containers/Cart.js\");\n/* harmony import */ var _client_components_DropDownMenu__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../client/components/DropDownMenu */ \"./src/client/components/DropDownMenu.js\");\n\n\n\n\n\n\n\n\n\n\n\n\n\n\nvar App =\n/*#__PURE__*/\nfunction (_Component) {\n  _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_4___default()(App, _Component);\n\n  function App() {\n    _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default()(this, App);\n\n    return _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2___default()(this, _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_3___default()(App).apply(this, arguments));\n  }\n\n  _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1___default()(App, [{\n    key: \"render\",\n    value: function render() {\n      return react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement(\"div\", null, react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement(_client_components_Navbar__WEBPACK_IMPORTED_MODULE_8__[\"default\"], {\n        badgeCount: this.props.badgeCount,\n        categories: this.props.categories,\n        selectedCategory: this.props.selectedCategory\n      }), react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement(_client_components_DropDownMenu__WEBPACK_IMPORTED_MODULE_12__[\"default\"], {\n        badgeCount: this.props.badgeCount,\n        categories: this.props.categories,\n        selectedCategory: this.props.selectedCategory\n      }), react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement(\"div\", {\n        className: \"content\"\n      }, react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_6__[\"Switch\"], null, react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_6__[\"Route\"], {\n        exact: true,\n        path: \"/category/:product?\",\n        name: \"Home\",\n        component: _client_containers_CategoryPage__WEBPACK_IMPORTED_MODULE_9__[\"default\"]\n      }), react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_6__[\"Route\"], {\n        path: \"/product/:name?\",\n        name: \"Product\",\n        component: _client_containers_Product__WEBPACK_IMPORTED_MODULE_10__[\"default\"]\n      }), react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_6__[\"Route\"], {\n        path: \"/cart\",\n        name: \"Cart\",\n        component: _client_containers_Cart__WEBPACK_IMPORTED_MODULE_11__[\"default\"]\n      }), react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_6__[\"Redirect\"], {\n        from: \"/\",\n        to: \"/category/:product?\"\n      }))));\n    }\n  }]);\n\n  return App;\n}(react__WEBPACK_IMPORTED_MODULE_5__[\"Component\"]);\n\nvar mapStateToProps = function mapStateToProps(state) {\n  return {\n    categories: state.products.categories,\n    selectedCategory: state.products.selectedCategory,\n    badgeCount: state.products.badgeCount\n  };\n};\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (Object(react_router_dom__WEBPACK_IMPORTED_MODULE_6__[\"withRouter\"])(Object(react_redux__WEBPACK_IMPORTED_MODULE_7__[\"connect\"])(mapStateToProps, {})(App)));\n\n//# sourceURL=webpack:///./src/shared/App.js?");

/***/ }),

/***/ "./src/shared/actions/index.js":
/*!*************************************!*\
  !*** ./src/shared/actions/index.js ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var axios_index__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios/index */ \"axios/index\");\n/* harmony import */ var axios_index__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios_index__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./types */ \"./src/shared/actions/types.js\");\n\n\nvar baseUri = 'http://localhost:3002';\n/* harmony default export */ __webpack_exports__[\"default\"] = ({\n  fetchAllProducts: function fetchAllProducts(_ref) {\n    var _ref$param = _ref.param,\n        param = _ref$param === void 0 ? \"\" : _ref$param;\n    return function (dispatch) {\n      axios_index__WEBPACK_IMPORTED_MODULE_0___default.a.get(\"https://my-json-server.typicode.com/spokeldn/react-test/db\").then(function (_ref2) {\n        var data = _ref2.data;\n        var filteredByCategory = [];\n        data.products.forEach(function (product) {\n          if (filteredByCategory.indexOf(product.category) === -1) {\n            filteredByCategory.push(product.category);\n          }\n        });\n        dispatch({\n          type: _types__WEBPACK_IMPORTED_MODULE_1__[\"PRODUCTS_TYPES\"].CATEGORIES_RECEIVED,\n          payload: filteredByCategory,\n          param: param || filteredByCategory[0]\n        });\n        dispatch({\n          type: _types__WEBPACK_IMPORTED_MODULE_1__[\"PRODUCTS_TYPES\"].PRODUCTS_RECEIVED,\n          payload: data.products\n        });\n        dispatch({\n          type: _types__WEBPACK_IMPORTED_MODULE_1__[\"PRODUCTS_TYPES\"].CATEGORY_SELECTED,\n          param: param\n        });\n        dispatch({\n          type: _types__WEBPACK_IMPORTED_MODULE_1__[\"PRODUCTS_TYPES\"].PRODUCTS_FINISHED,\n          payload: true\n        });\n      })[\"catch\"](function (e) {\n        console.warn(e);\n      });\n    };\n  },\n  fetchProductsByCategory: function fetchProductsByCategory() {\n    var category = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : \"\";\n    return function (dispatch) {\n      dispatch({\n        type: _types__WEBPACK_IMPORTED_MODULE_1__[\"PRODUCTS_TYPES\"].CATEGORY_SELECTED,\n        param: category\n      });\n    };\n  },\n  filterCategoryProducts: function filterCategoryProducts(category, text) {\n    return function (dispatch) {\n      dispatch({\n        type: _types__WEBPACK_IMPORTED_MODULE_1__[\"PRODUCTS_TYPES\"].FILTER_PRODUCTS,\n        category: category,\n        text: text\n      });\n    };\n  },\n  fetchProductDetails: function fetchProductDetails(category, product) {\n    return function (dispatch) {\n      dispatch({\n        type: _types__WEBPACK_IMPORTED_MODULE_1__[\"PRODUCTS_TYPES\"].FETCH_PRODUCT,\n        category: category,\n        product: product\n      });\n    };\n  },\n  addProductToCart: function addProductToCart(category, product) {\n    return function (dispatch) {\n      dispatch({\n        type: _types__WEBPACK_IMPORTED_MODULE_1__[\"PRODUCTS_TYPES\"].ADD_PRODUCT_CART,\n        category: category,\n        product: product\n      });\n    };\n  },\n  fetchCart: function fetchCart(category, product) {\n    return function (dispatch) {\n      dispatch({\n        type: _types__WEBPACK_IMPORTED_MODULE_1__[\"PRODUCTS_TYPES\"].FETCH_CART,\n        category: category,\n        product: product\n      });\n    };\n  },\n  deleteProductFromCart: function deleteProductFromCart(category, product) {\n    return function (dispatch) {\n      dispatch({\n        type: _types__WEBPACK_IMPORTED_MODULE_1__[\"PRODUCTS_TYPES\"].DELETE_PRODUCT_CART,\n        category: category,\n        product: product\n      });\n    };\n  },\n  addDiscount: function addDiscount(code) {\n    return function (dispatch) {\n      dispatch({\n        type: _types__WEBPACK_IMPORTED_MODULE_1__[\"PRODUCTS_TYPES\"].ADD_DISCOUNT,\n        code: code\n      });\n    };\n  }\n});\n\n//# sourceURL=webpack:///./src/shared/actions/index.js?");

/***/ }),

/***/ "./src/shared/actions/types.js":
/*!*************************************!*\
  !*** ./src/shared/actions/types.js ***!
  \*************************************/
/*! exports provided: PRODUCTS_TYPES */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"PRODUCTS_TYPES\", function() { return PRODUCTS_TYPES; });\nvar PRODUCTS_TYPES = {\n  PRODUCTS_REQUESTED: 'products_requested',\n  PRODUCTS_RECEIVED: 'products_received',\n  PRODUCTS_FINISHED: 'products_finished',\n  CATEGORIES_RECEIVED: 'categories_received',\n  CATEGORY_SELECTED: 'category_selected',\n  UPDATE_DATA: 'update_data',\n  FILTER_PRODUCTS: 'filter_products',\n  FETCH_PRODUCT: 'fetch_product',\n  ADD_PRODUCT_CART: 'add_product_cart',\n  FETCH_CART: 'fetch_cart',\n  DELETE_PRODUCT_CART: 'delete_product_cart',\n  ADD_DISCOUNT: 'add_product'\n};\n\n//# sourceURL=webpack:///./src/shared/actions/types.js?");

/***/ }),

/***/ "./src/shared/reducers/configureStore.js":
/*!***********************************************!*\
  !*** ./src/shared/reducers/configureStore.js ***!
  \***********************************************/
/*! exports provided: configureStore */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"configureStore\", function() { return configureStore; });\n/* harmony import */ var redux__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! redux */ \"redux\");\n/* harmony import */ var redux__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(redux__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var redux_thunk__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! redux-thunk */ \"redux-thunk\");\n/* harmony import */ var redux_thunk__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(redux_thunk__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var _index__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./index */ \"./src/shared/reducers/index.js\");\n\n\n\nvar configureStore = function configureStore() {\n  var initialState = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};\n  var createStoreWithMiddleware = Object(redux__WEBPACK_IMPORTED_MODULE_0__[\"applyMiddleware\"])(redux_thunk__WEBPACK_IMPORTED_MODULE_1___default.a)(redux__WEBPACK_IMPORTED_MODULE_0__[\"createStore\"]);\n  return createStoreWithMiddleware(_index__WEBPACK_IMPORTED_MODULE_2__[\"default\"], initialState);\n};\n\n//# sourceURL=webpack:///./src/shared/reducers/configureStore.js?");

/***/ }),

/***/ "./src/shared/reducers/index.js":
/*!**************************************!*\
  !*** ./src/shared/reducers/index.js ***!
  \**************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var redux__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! redux */ \"redux\");\n/* harmony import */ var redux__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(redux__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _productReducer__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./productReducer */ \"./src/shared/reducers/productReducer.js\");\n\n\nvar rootReducer = Object(redux__WEBPACK_IMPORTED_MODULE_0__[\"combineReducers\"])({\n  products: _productReducer__WEBPACK_IMPORTED_MODULE_1__[\"productReducer\"]\n});\n/* harmony default export */ __webpack_exports__[\"default\"] = (rootReducer);\n\n//# sourceURL=webpack:///./src/shared/reducers/index.js?");

/***/ }),

/***/ "./src/shared/reducers/productReducer.js":
/*!***********************************************!*\
  !*** ./src/shared/reducers/productReducer.js ***!
  \***********************************************/
/*! exports provided: productReducer */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"productReducer\", function() { return productReducer; });\n/* harmony import */ var _babel_runtime_helpers_toConsumableArray__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/toConsumableArray */ \"@babel/runtime/helpers/toConsumableArray\");\n/* harmony import */ var _babel_runtime_helpers_toConsumableArray__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_toConsumableArray__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/helpers/defineProperty */ \"@babel/runtime/helpers/defineProperty\");\n/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var _babel_runtime_helpers_objectSpread__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime/helpers/objectSpread */ \"@babel/runtime/helpers/objectSpread\");\n/* harmony import */ var _babel_runtime_helpers_objectSpread__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_objectSpread__WEBPACK_IMPORTED_MODULE_2__);\n/* harmony import */ var _actions_types__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../actions/types */ \"./src/shared/actions/types.js\");\n\n\n\n\nvar INITIAL_STATE = {\n  data: [],\n  cart: [],\n  cartTotal: 0.00,\n  discountCartTotal: 0.00,\n  categories: [],\n  selectedCategory: \"\",\n  selectedCategoryData: [],\n  badgeCount: 0,\n  productDetails: {},\n  finishedFetching: false,\n  discountCode: \"CODE20\",\n  userDiscountCode: \"\"\n};\nvar productReducer = function productReducer() {\n  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : INITIAL_STATE;\n  var action = arguments.length > 1 ? arguments[1] : undefined;\n\n  switch (action.type) {\n    case _actions_types__WEBPACK_IMPORTED_MODULE_3__[\"PRODUCTS_TYPES\"].CATEGORIES_RECEIVED:\n      return _babel_runtime_helpers_objectSpread__WEBPACK_IMPORTED_MODULE_2___default()({}, state, {\n        categories: action.payload,\n        selectedCategory: action.param || state.selectedCategory\n      });\n\n    case _actions_types__WEBPACK_IMPORTED_MODULE_3__[\"PRODUCTS_TYPES\"].CATEGORY_SELECTED:\n      var param = action.param || state.selectedCategory;\n\n      if (!param) {\n        param = state.categories[0];\n      }\n\n      var filteredByCategory = state.data.filter(function (product) {\n        return product.category === param;\n      });\n      return _babel_runtime_helpers_objectSpread__WEBPACK_IMPORTED_MODULE_2___default()({}, state, {\n        selectedCategoryData: filteredByCategory,\n        selectedCategory: param\n      });\n\n    case _actions_types__WEBPACK_IMPORTED_MODULE_3__[\"PRODUCTS_TYPES\"].PRODUCTS_RECEIVED:\n      return _babel_runtime_helpers_objectSpread__WEBPACK_IMPORTED_MODULE_2___default()({}, state, {\n        data: action.payload\n      });\n\n    case _actions_types__WEBPACK_IMPORTED_MODULE_3__[\"PRODUCTS_TYPES\"].PRODUCTS_FINISHED:\n      return _babel_runtime_helpers_objectSpread__WEBPACK_IMPORTED_MODULE_2___default()({}, state, {\n        finishedFetching: action.payload\n      });\n\n    case _actions_types__WEBPACK_IMPORTED_MODULE_3__[\"PRODUCTS_TYPES\"].FILTER_PRODUCTS:\n      var filteredProduct = state.data.filter(function (product) {\n        return product.category === action.category && product.title.toUpperCase().indexOf(action.text.toUpperCase()) > -1;\n      });\n      return _babel_runtime_helpers_objectSpread__WEBPACK_IMPORTED_MODULE_2___default()({}, state, {\n        selectedCategoryData: filteredProduct\n      });\n\n    case _actions_types__WEBPACK_IMPORTED_MODULE_3__[\"PRODUCTS_TYPES\"].FETCH_PRODUCT:\n      var product = state.data.filter(function (product) {\n        return product.category === action.category && product.title === action.product;\n      });\n      return _babel_runtime_helpers_objectSpread__WEBPACK_IMPORTED_MODULE_2___default()({}, state, {\n        productDetails: product[0]\n      });\n\n    case _actions_types__WEBPACK_IMPORTED_MODULE_3__[\"PRODUCTS_TYPES\"].ADD_PRODUCT_CART:\n      var selectedIndex = state.data.findIndex(function (product) {\n        return product.category === action.category && product.title === action.product && product.amount > 0;\n      });\n      var selectedProduct = state.data[selectedIndex];\n      state.data[selectedIndex].amount -= 1;\n      state.badgeCount += 1;\n      var cartIndex = state.cart.indexOf(selectedProduct);\n\n      if (cartIndex !== -1) {\n        state.cart[cartIndex].quatity += 1;\n        state.cart[cartIndex].quatity_total += state.data[selectedIndex].price;\n      } else {\n        selectedProduct['quatity'] = 1;\n        selectedProduct['quatity_total'] = state.data[selectedIndex].price;\n        state.cart.push(selectedProduct);\n      }\n\n      var total = 0;\n      state.cart.map(function (product) {\n        total += product.quatity_total;\n      });\n      return _babel_runtime_helpers_objectSpread__WEBPACK_IMPORTED_MODULE_2___default()({}, state, {\n        productDetails: _babel_runtime_helpers_objectSpread__WEBPACK_IMPORTED_MODULE_2___default()({}, state.productDetails, _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_1___default()({}, 'amount', state.data[selectedIndex].amount)),\n        cartTotal: total.toFixed(2)\n      });\n\n    case _actions_types__WEBPACK_IMPORTED_MODULE_3__[\"PRODUCTS_TYPES\"].DELETE_PRODUCT_CART:\n      var updateQuatityIndex = state.data.findIndex(function (product) {\n        return product.category === action.category && product.title === action.product;\n      });\n      state.data[updateQuatityIndex].amount += 1;\n      state.badgeCount -= 1;\n      var removeIndex = state.cart.findIndex(function (product) {\n        return product.category === action.category && product.title === action.product;\n      });\n      var cart = state.cart;\n      state.cartTotal -= state.data[updateQuatityIndex].price;\n      cart[removeIndex].quatity -= 1;\n\n      if (cart[removeIndex].quatity === 0) {\n        cart.splice(removeIndex, 1);\n      } else {\n        cart[removeIndex].quatity_total -= state.data[updateQuatityIndex].price;\n      }\n\n      return _babel_runtime_helpers_objectSpread__WEBPACK_IMPORTED_MODULE_2___default()({}, state, {\n        cart: _babel_runtime_helpers_toConsumableArray__WEBPACK_IMPORTED_MODULE_0___default()(cart)\n      });\n\n    default:\n      return state;\n  }\n};\n\n//# sourceURL=webpack:///./src/shared/reducers/productReducer.js?");

/***/ }),

/***/ "./src/shared/router/routes.js":
/*!*************************************!*\
  !*** ./src/shared/router/routes.js ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _client_containers_CategoryPage__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../client/containers/CategoryPage */ \"./src/client/containers/CategoryPage.js\");\n\n/* harmony default export */ __webpack_exports__[\"default\"] = ([{\n  path: '*',\n  component: _client_containers_CategoryPage__WEBPACK_IMPORTED_MODULE_0__[\"default\"],\n  exact: true,\n  isStatic: true\n}, {\n  path: '/category/:product?',\n  component: _client_containers_CategoryPage__WEBPACK_IMPORTED_MODULE_0__[\"default\"],\n  exact: true,\n  isStatic: true\n}]);\n\n//# sourceURL=webpack:///./src/shared/router/routes.js?");

/***/ }),

/***/ "@babel/runtime/helpers/assertThisInitialized":
/*!***************************************************************!*\
  !*** external "@babel/runtime/helpers/assertThisInitialized" ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"@babel/runtime/helpers/assertThisInitialized\");\n\n//# sourceURL=webpack:///external_%22@babel/runtime/helpers/assertThisInitialized%22?");

/***/ }),

/***/ "@babel/runtime/helpers/asyncToGenerator":
/*!**********************************************************!*\
  !*** external "@babel/runtime/helpers/asyncToGenerator" ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"@babel/runtime/helpers/asyncToGenerator\");\n\n//# sourceURL=webpack:///external_%22@babel/runtime/helpers/asyncToGenerator%22?");

/***/ }),

/***/ "@babel/runtime/helpers/classCallCheck":
/*!********************************************************!*\
  !*** external "@babel/runtime/helpers/classCallCheck" ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"@babel/runtime/helpers/classCallCheck\");\n\n//# sourceURL=webpack:///external_%22@babel/runtime/helpers/classCallCheck%22?");

/***/ }),

/***/ "@babel/runtime/helpers/createClass":
/*!*****************************************************!*\
  !*** external "@babel/runtime/helpers/createClass" ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"@babel/runtime/helpers/createClass\");\n\n//# sourceURL=webpack:///external_%22@babel/runtime/helpers/createClass%22?");

/***/ }),

/***/ "@babel/runtime/helpers/defineProperty":
/*!********************************************************!*\
  !*** external "@babel/runtime/helpers/defineProperty" ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"@babel/runtime/helpers/defineProperty\");\n\n//# sourceURL=webpack:///external_%22@babel/runtime/helpers/defineProperty%22?");

/***/ }),

/***/ "@babel/runtime/helpers/extends":
/*!*************************************************!*\
  !*** external "@babel/runtime/helpers/extends" ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"@babel/runtime/helpers/extends\");\n\n//# sourceURL=webpack:///external_%22@babel/runtime/helpers/extends%22?");

/***/ }),

/***/ "@babel/runtime/helpers/getPrototypeOf":
/*!********************************************************!*\
  !*** external "@babel/runtime/helpers/getPrototypeOf" ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"@babel/runtime/helpers/getPrototypeOf\");\n\n//# sourceURL=webpack:///external_%22@babel/runtime/helpers/getPrototypeOf%22?");

/***/ }),

/***/ "@babel/runtime/helpers/inherits":
/*!**************************************************!*\
  !*** external "@babel/runtime/helpers/inherits" ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"@babel/runtime/helpers/inherits\");\n\n//# sourceURL=webpack:///external_%22@babel/runtime/helpers/inherits%22?");

/***/ }),

/***/ "@babel/runtime/helpers/objectSpread":
/*!******************************************************!*\
  !*** external "@babel/runtime/helpers/objectSpread" ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"@babel/runtime/helpers/objectSpread\");\n\n//# sourceURL=webpack:///external_%22@babel/runtime/helpers/objectSpread%22?");

/***/ }),

/***/ "@babel/runtime/helpers/possibleConstructorReturn":
/*!*******************************************************************!*\
  !*** external "@babel/runtime/helpers/possibleConstructorReturn" ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"@babel/runtime/helpers/possibleConstructorReturn\");\n\n//# sourceURL=webpack:///external_%22@babel/runtime/helpers/possibleConstructorReturn%22?");

/***/ }),

/***/ "@babel/runtime/helpers/slicedToArray":
/*!*******************************************************!*\
  !*** external "@babel/runtime/helpers/slicedToArray" ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"@babel/runtime/helpers/slicedToArray\");\n\n//# sourceURL=webpack:///external_%22@babel/runtime/helpers/slicedToArray%22?");

/***/ }),

/***/ "@babel/runtime/helpers/toConsumableArray":
/*!***********************************************************!*\
  !*** external "@babel/runtime/helpers/toConsumableArray" ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"@babel/runtime/helpers/toConsumableArray\");\n\n//# sourceURL=webpack:///external_%22@babel/runtime/helpers/toConsumableArray%22?");

/***/ }),

/***/ "@babel/runtime/regenerator":
/*!*********************************************!*\
  !*** external "@babel/runtime/regenerator" ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"@babel/runtime/regenerator\");\n\n//# sourceURL=webpack:///external_%22@babel/runtime/regenerator%22?");

/***/ }),

/***/ "axios/index":
/*!******************************!*\
  !*** external "axios/index" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"axios/index\");\n\n//# sourceURL=webpack:///external_%22axios/index%22?");

/***/ }),

/***/ "cors":
/*!***********************!*\
  !*** external "cors" ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"cors\");\n\n//# sourceURL=webpack:///external_%22cors%22?");

/***/ }),

/***/ "express":
/*!**************************!*\
  !*** external "express" ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"express\");\n\n//# sourceURL=webpack:///external_%22express%22?");

/***/ }),

/***/ "path":
/*!***********************!*\
  !*** external "path" ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"path\");\n\n//# sourceURL=webpack:///external_%22path%22?");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"react\");\n\n//# sourceURL=webpack:///external_%22react%22?");

/***/ }),

/***/ "react-dom/server":
/*!***********************************!*\
  !*** external "react-dom/server" ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"react-dom/server\");\n\n//# sourceURL=webpack:///external_%22react-dom/server%22?");

/***/ }),

/***/ "react-redux":
/*!******************************!*\
  !*** external "react-redux" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"react-redux\");\n\n//# sourceURL=webpack:///external_%22react-redux%22?");

/***/ }),

/***/ "react-router-dom":
/*!***********************************!*\
  !*** external "react-router-dom" ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"react-router-dom\");\n\n//# sourceURL=webpack:///external_%22react-router-dom%22?");

/***/ }),

/***/ "redux":
/*!************************!*\
  !*** external "redux" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"redux\");\n\n//# sourceURL=webpack:///external_%22redux%22?");

/***/ }),

/***/ "redux-thunk":
/*!******************************!*\
  !*** external "redux-thunk" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"redux-thunk\");\n\n//# sourceURL=webpack:///external_%22redux-thunk%22?");

/***/ })

/******/ });