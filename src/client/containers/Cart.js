import React, { Component } from 'react';
import { connect } from 'react-redux';
import CartProducts from '../components/CartProducts';
import api_action from '../../shared/actions';

class Cart extends Component {

    discountPurchase = (event) => {
        this.props.addDiscount(event.target.value);
    };

    removeProduct = (catgegory, product, quatity) => {
        this.props.deleteProductFromCart(catgegory, product, quatity);
    };

    render() {
        return (
            <CartProducts
                cart={this.props.cart}
                cartTotal={this.props.cartTotal}
                discountCartTotal={this.props.discountCartTotal}
                discountCode={this.props.discountCode}
                deleteProductFromCart={this.removeProduct}
                addDiscount={this.discountPurchase}
            />
        );
    }
}

const mapStateToProps = (state) => {
    return {
        cart: state.products.cart,
        cartTotal: state.products.cartTotal,
        discountCartTotal: state.products.discountCartTotal,
        discountCode: state.products.discountCode,
    }
};

const mapDispatchToProps = (dispatch) => ({
    deleteProductFromCart: (category, product, quatity) => dispatch(api_action.deleteProductFromCart(category, product, quatity)),
    addDiscount: (code) => dispatch(api_action.addDiscount(code)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Cart);