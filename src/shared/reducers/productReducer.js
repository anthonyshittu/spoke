import {
    PRODUCTS_TYPES
} from '../actions/types';

const INITIAL_STATE = {
    data: [],
    cart: [],
    cartTotal: 0.00,
    discountCartTotal: 0.00,
    categories: [],
    selectedCategory: "",
    selectedCategoryData: [],
    badgeCount: 0,
    productDetails: {},
    finishedFetching: false,
    discountCode: "CODE20",
    userDiscountCode: ""
};

export const productReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case PRODUCTS_TYPES.CATEGORIES_RECEIVED:
            return {
                ...state,
                categories: action.payload,
                selectedCategory: action.param || state.selectedCategory
            };
        case PRODUCTS_TYPES.CATEGORY_SELECTED:
            let param = action.param || state.selectedCategory;
            if (!param) {
                param = state.categories[0]
            }
            let filteredByCategory = state.data.filter((product) => {
                return product.category === param;
            });
            return {
                ...state,
                selectedCategoryData: filteredByCategory,
                selectedCategory: param
            };
        case PRODUCTS_TYPES.PRODUCTS_RECEIVED:
            return {
                ...state,
                data: action.payload,
            };
        case PRODUCTS_TYPES.PRODUCTS_FINISHED:
            return {
                ...state,
                finishedFetching: action.payload
            };
        case PRODUCTS_TYPES.FILTER_PRODUCTS:
            let filteredProduct = state.data.filter((product) => {
                return (
                    product.category === action.category
                    &&
                    product.title.toUpperCase().indexOf(action.text.toUpperCase()) > -1
                );
            });
            return {
                ...state,
                selectedCategoryData: filteredProduct
            };
        case PRODUCTS_TYPES.FETCH_PRODUCT:
            let product = state.data.filter((product) => {
                return (product.category === action.category && product.title === action.product);
            });
            return {
                ...state,
                productDetails: product[0]
            };
        case PRODUCTS_TYPES.ADD_PRODUCT_CART:
            const selectedIndex = state.data.findIndex(product =>
                product.category === action.category &&
                product.title === action.product &&
                product.amount > 0
            );

            const selectedProduct = state.data[selectedIndex];
            state.data[selectedIndex].amount -= 1;
            state.badgeCount += 1;

            const cartIndex = state.cart.indexOf(selectedProduct);
            if (cartIndex !== -1) {
                state.cart[cartIndex].quatity += 1;
                state.cart[cartIndex].quatity_total += state.data[selectedIndex].price;
            } else {
                selectedProduct['quatity'] = 1;
                selectedProduct['quatity_total'] = state.data[selectedIndex].price;
                state.cart.push(selectedProduct);
            }

            let total = 0;
            state.cart.map(product => {
                total += product.quatity_total
            });

            return {
                ...state,
                productDetails: {...state.productDetails, ['amount']: state.data[selectedIndex].amount},
                cartTotal: total.toFixed(2)
            };
        case PRODUCTS_TYPES.DELETE_PRODUCT_CART:
            const updateQuatityIndex = state.data.findIndex(product =>
                product.category === action.category &&
                product.title === action.product
            );

            state.data[updateQuatityIndex].amount += 1;
            state.badgeCount -= 1;

            const removeIndex = state.cart.findIndex(product =>
                product.category === action.category &&
                product.title === action.product
            );

            const cart = state.cart;
            state.cartTotal -=  state.data[updateQuatityIndex].price;
            cart[removeIndex].quatity -= 1;

            if ( cart[removeIndex].quatity === 0) {
                cart.splice(removeIndex, 1)
            } else {
                cart[removeIndex].quatity_total -= state.data[updateQuatityIndex].price;
            }

            return {
                ...state,
                cart: [...cart]
            };
        default:
            return state;
    }
};