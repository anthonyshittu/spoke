import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Router, Route, Switch } from 'react-router-dom';
import { createBrowserHistory } from 'history';
import {configureStore} from '../shared/reducers/configureStore';
import '../scss/style.scss';
import App from '../shared/App';


let data = {products: {}};
// data = window.__SERIALIZED_DATA__;
// delete window.__SERIALIZED_DATA__;
if ('products' in window.__SERIALIZED_DATA__) {
    data.products = window.__SERIALIZED_DATA__.products;
    delete window.__SERIALIZED_DATA__;
}

const store = configureStore(data);
const history = createBrowserHistory();

ReactDOM.hydrate(
    <Provider store={store}>
        <Router history={history}>
            <App/>
        </Router>
    </Provider>,
    document.querySelector('#app')
);