export default (store, markup) => `
  <!DOCTYPE html>
  <html>
    <head>
      <script>window.__SERIALIZED_DATA__ = ${JSON.stringify(store.getState())}</script>
    </head>
    <body>
      <div id="app">${markup}</div>
      <script src="/bundle.js" type="text/javascript"></script>
    </body>
  </html>
`;