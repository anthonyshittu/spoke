import React from 'react';
import { mount } from 'enzyme';
import toJson from 'enzyme-to-json';
import { BrowserRouter as Router } from 'react-router-dom';
import CartProducts from '../../client/components/CartProducts';

let cart= [
    {
        "title": "Heroes",
        "category": "Trousers",
        "description": "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.",
        "amount": 2,
        "price": 12.99,
        "image": "https://dummyimage.com/300x200/9e3d9e/363ead",
        "new": true,
        "quatity": 2
    },
    {
        "title": "Sporty",
        "category": "Polos",
        "description": "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.",
        "amount": 1,
        "price": 61.49,
        "image": "https://dummyimage.com/300x200/9e3d9e/363ead",
        "new": true,
        "quatity": 1
    }
];
const cartTotal= 87.47;
const discountCartTotal= 77.47;
const deleteProductFromCart= jest.fn();
const addProductCart= jest.fn();

describe('<CartProducts />', () => {
    test('test display cart with products', () => {
        const component = mount(
            <Router>
                <CartProducts
                    cart={cart}
                    cartTotal={cartTotal}
                    discountCartTotal={discountCartTotal}
                    deleteProductFromCart={deleteProductFromCart}
                    addProductToCart={addProductCart}
                />
            </Router>
        );
        expect(component.find('input').length).toEqual(4);
        expect(component.find('img').length).toEqual(2);
    });

    test('test cart with no product', () => {
        cart= [];
        const cartTotal= 0.00;
        const discountCartTotal= 0.00;
        const component = mount(
            <Router>
                <CartProducts
                    cart={cart}
                    cartTotal={cartTotal}
                    discountCartTotal={discountCartTotal}
                    deleteProductFromCart={deleteProductFromCart}
                    addProductToCart={addProductCart}
                />
            </Router>
        );
        expect(component.find('div').length).toEqual(2);
        expect(component.find('input').length).toEqual(0);
    });
});