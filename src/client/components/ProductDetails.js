import React, { useState } from 'react';
import Image from './Image';

function ProductDetails ({ title, price, description, image, amount, addProductToCart }) {
    const [ellipsis, setValue] = useState(false);

    return (title ?
        <div className="product-container">
            <div className="product-thumb">
                <Image url={image} alt={title} width={200} height={200}/>
            </div>
            <div className="product-content">
                <h3 className="product-title">£{price}</h3>
                <div className={ellipsis ? "product-desc product-desc-no-ellipsis" : "product-desc product-desc-ellipsis"} onClick={() => setValue(!ellipsis)}>{description}</div>
                {
                    amount === 0 ?
                        <p>Stock not available</p>
                        :
                        <input type="button" value="Add to Cart" className="btn default" onClick={() => {
                            addProductToCart()
                        }}/>
                }
            </div>
        </div>
        :
        <p>An error as occurred</p>
    )
};

export default ProductDetails;