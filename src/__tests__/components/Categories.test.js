import React from 'react';
import { shallow, mount } from 'enzyme';
import toJson from 'enzyme-to-json';
import { BrowserRouter as Router } from 'react-router-dom';

import Categories from '../../client/components/Categories';
const products = [
    {
        "title": "Heroes",
        "category": "Trousers",
        "description": "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.",
        "amount": 2,
        "price": 12.99,
        "image": "https://dummyimage.com/300x200/9e3d9e/363ead",
        "new": true
    },
    {
        "title": "Sharps",
        "category": "Trousers",
        "description": "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.",
        "amount": 6,
        "price": 45.99,
        "image": "https://dummyimage.com/300x200/9e3d9e/363ead",
        "new": false
    },
    {
        "title": "Dessert sand",
        "category": "Jeans",
        "description": "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.",
        "amount": 2,
        "price": 66.49,
        "image": "https://dummyimage.com/300x200/9e3d9e/363ead",
        "new": true
    },
    {
        "title": "Blue wave",
        "category": "Jeans",
        "description": "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.",
        "amount": 1,
        "price": 34.99,
        "image": "https://dummyimage.com/300x200/9e3d9e/363ead",
        "new": false
    },
    {
        "title": "Snack",
        "category": "Jeans",
        "description": "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.",
        "amount": 3,
        "price": 33.99,
        "image": "https://dummyimage.com/300x200/9e3d9e/363ead",
        "new": false
    },
    {
        "title": "Kids",
        "category": "Polos",
        "description": "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.",
        "amount": 15,
        "price": 24.99,
        "image": "https://dummyimage.com/300x200/9e3d9e/363ead",
        "new": true
    },
    {
        "title": "Valentines",
        "category": "Polos",
        "description": "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.",
        "amount": 23,
        "price": 12.99,
        "image": "https://dummyimage.com/300x200/9e3d9e/363ead",
        "new": true
    },
    {
        "title": "Christmas",
        "category": "Gifts",
        "description": "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.",
        "amount": 6,
        "price": 12.49,
        "image": "https://dummyimage.com/300x200/9e3d9e/363ead",
        "new": false
    },
    {
        "title": "Baker",
        "category": "Gifts",
        "description": "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.",
        "amount": 7,
        "price": 26.99,
        "image": "https://dummyimage.com/300x200/9e3d9e/363ead",
        "new": false
    },
    {
        "title": "Launch",
        "category": "Shorts",
        "description": "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.",
        "amount": 7,
        "price": 14.49,
        "image": "https://dummyimage.com/300x200/9e3d9e/363ead",
        "new": true
    },
    {
        "title": "Christmas md",
        "category": "Shorts",
        "description": "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.",
        "amount": 4,
        "price": 11.49,
        "image": "https://dummyimage.com/300x200/9e3d9e/363ead",
        "new": false
    },
    {
        "title": "Green dots",
        "category": "Trousers",
        "description": "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.",
        "amount": 1,
        "price": 9.99,
        "image": "https://dummyimage.com/300x200/9e3d9e/363ead",
        "new": false
    },
    {
        "title": "Mini mate",
        "category": "Shorts",
        "description": "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.",
        "amount": 5,
        "price": 25.99,
        "image": "https://dummyimage.com/300x200/9e3d9e/363ead",
        "new": false
    },
    {
        "title": "Sporty",
        "category": "Polos",
        "description": "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.",
        "amount": 1,
        "price": 61.49,
        "image": "https://dummyimage.com/300x200/9e3d9e/363ead",
        "new": true
    }
];
const category = 'Trousers';
describe('<Categories />', () => {
    test('test categories component', () => {
        const component = mount(
            <Router>
                <Categories
                    products={products}
                    category={category}
                />
            </Router>
        );
        expect(component.find('Link').length).toEqual(3)

    });
});