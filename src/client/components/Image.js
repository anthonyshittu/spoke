import React from 'react';

const Image = ({ alt, url, width, height }) => (
    <img
        src={url}
        width={width}
        height={height}
        alt={alt}
    />
);

export default Image;