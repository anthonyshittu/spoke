import express from 'express';
import cors from 'cors';
import React from 'react';
import { renderToString } from 'react-dom/server';
import { matchPath, StaticRouter } from 'react-router-dom';
import path from 'path'
import { Provider } from 'react-redux';
import {configureStore} from '../shared/reducers/configureStore';
import App from '../shared/App';
import routes from '../shared/router/routes';
import template from './template';

const app = express();

app.use(cors());

app.use(express.static('public'));
const delay = ms => new Promise(res => setTimeout(res, ms));

app.get('/favicon.ico', (req, res) => res.status(204));
app.get('*', async (req, res, next) => {
    let store = configureStore();
    const activeRoute = routes.find(path => matchPath(req.path, path)) || {};
    let param;
    if (req.params[0].split("/")[2] !== 'undefined') {
        param = req.params[0].split("/")[2]
    }
    if ('isStatic' in activeRoute) {
        activeRoute.component.fetching({...store, param: param})
    }
    await delay(1000);
    const markup = renderToString(
        <Provider store={store}>
            <StaticRouter location={req.url} context={store.getState()}>
                <App />
            </StaticRouter>
        </Provider>
    );

    res.send(template(store, markup));
});


const PORT = 2040;

app.listen(PORT, () => {
    console.log(`Server is listening on port ${PORT}`);
});